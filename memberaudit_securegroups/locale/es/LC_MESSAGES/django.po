# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Zigor Fernandez Moreno <sietehierros@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-07-27 21:18+0200\n"
"PO-Revision-Date: 2024-05-10 14:08+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Spanish <https://weblate.ppfeufer.de/projects/alliance-auth-"
"apps/aa-member-audit-secure-groups/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.5.3\n"

#: memberaudit_securegroups/admin.py:83
#, python-brace-format
msgid "{inactivity_threshold:d} day"
msgid_plural "{inactivity_threshold:d} days"
msgstr[0] "día {inactivity_threshold:d}"
msgstr[1] "días {inactivity_threshold:d}"

#: memberaudit_securegroups/admin.py:147
msgid "corporation role"
msgstr ""

#: memberaudit_securegroups/apps.py:20
#, fuzzy, python-brace-format
#| msgid "Member Audit Secure Groups Integration v{__version__}"
msgid "Secure Groups (Member Audit Integration) v{__version__}"
msgstr ""
"Integración de la Auditoria de Miembros de los Grupos Seguros v{__version__}"

#: memberaudit_securegroups/models.py:114
msgid "The filter description that is shown to end users."
msgstr "La descripción del filtro que se muestra a los usuarios finales."

#: memberaudit_securegroups/models.py:139
#, fuzzy
#| msgid "Please Create a filter!"
msgid "Please create a filter!"
msgstr "¡Por favor crea un filtro!"

#: memberaudit_securegroups/models.py:147
#, fuzzy
#| msgid "Please Create an audit function!"
msgid "Please create an audit function!"
msgstr "¡Por favor cree una función de auditoría!"

#: memberaudit_securegroups/models.py:156
msgid "Maximum allowable inactivity, in <strong>days</strong>."
msgstr "Inactividad máxima permitida, en <strong> días </strong>."

#: memberaudit_securegroups/models.py:167
#, python-brace-format
msgid "{self.inactivity_threshold:d} day"
msgid_plural "{self.inactivity_threshold:d} days"
msgstr[0] "día {self.inactivity_threshold:d}"
msgstr[1] "días {self.inactivity_threshold:d}"

#: memberaudit_securegroups/models.py:172
#, python-brace-format
msgid "Activity [Last {inactivity_threshold}]"
msgstr "Actividad [por última vez {inactivity_threshold}]"

#: memberaudit_securegroups/models.py:224
#, fuzzy
#| msgid "Active Character: "
#| msgid_plural "Active Characters: "
msgid "Active character: "
msgid_plural "Active characters: "
msgstr[0] "Personaje Activo: "
msgstr[1] "Personajes Activos: "

#: memberaudit_securegroups/models.py:241
msgid "Minimum allowable age, in <strong>days</strong>."
msgstr "Edad mínima permitida, en <strong>días</strong>."

#: memberaudit_securegroups/models.py:252
#, python-brace-format
msgid "{self.age_threshold:d} day"
msgid_plural "{self.age_threshold:d} days"
msgstr[0] "día {self.age_threshold:d}"
msgstr[1] "días {self.age_threshold:d}"

#: memberaudit_securegroups/models.py:257
#, fuzzy, python-brace-format
#| msgid "Character Age [{age_threshold}]"
msgid "Character age [{age_threshold}]"
msgstr "Edad del Personaje [{age_threshold}]"

#: memberaudit_securegroups/models.py:315
msgid "User must possess <strong>one</strong> of the selected assets."
msgstr ""
"El usuario debe tener <strong>uno</strong> de los bienes seleccionados."

#: memberaudit_securegroups/models.py:325
msgid "Member Audit Asset"
msgstr "Auditoría de Bienes de los Miembros"

#: memberaudit_securegroups/models.py:403
msgid "Compliance"
msgstr "Cumplimiento"

#: memberaudit_securegroups/models.py:454
#, python-brace-format
msgid "All characters have been added to {MEMBERAUDIT_APP_NAME}"
msgstr "Todos los personajes están añadidos en {MEMBERAUDIT_APP_NAME}"

#: memberaudit_securegroups/models.py:460
msgid "Missing character: "
msgid_plural "Missing characters: "
msgstr[0] "Personaje que falta: "
msgstr[1] "Personajes que faltan: "

#: memberaudit_securegroups/models.py:482
msgid "The character with the role must be in one of these corporations."
msgstr ""

#: memberaudit_securegroups/models.py:489
msgid "User must have a character with this role."
msgstr ""

#: memberaudit_securegroups/models.py:494
msgid "When checked, the filter will also include the users alt-characters."
msgstr ""

#: memberaudit_securegroups/models.py:504
#, fuzzy
#| msgid "Member Audit Skill Set"
msgid "Member Audit Corporation Role"
msgstr "Auditoría de Miembro Conjunto de Habilidades"

#: memberaudit_securegroups/models.py:574
msgid "The character with the title must be in one of these corporations."
msgstr ""

#: memberaudit_securegroups/models.py:580
msgid "User must have a character with this title."
msgstr ""

#: memberaudit_securegroups/models.py:585
msgid "When True, the filter will also include the users alt-characters."
msgstr ""

#: memberaudit_securegroups/models.py:595
#, fuzzy
#| msgid "Member Audit Skill Set"
msgid "Member Audit Corporation Title"
msgstr "Auditoría de Miembro Conjunto de Habilidades"

#: memberaudit_securegroups/models.py:660
#, fuzzy
#| msgid "Minimum allowable skillpoints."
msgid "Minimum allowable skill points."
msgstr "Puntos de habilidad mínimos permitidos."

#: memberaudit_securegroups/models.py:673
#, python-brace-format
msgid "{sp_threshold} skill point"
msgid_plural "{sp_threshold} skill points"
msgstr[0] "punto de habilidad {sp_threshold}"
msgstr[1] "puntos de habilidad {sp_threshold}"

#: memberaudit_securegroups/models.py:678
#, python-brace-format
msgid "Member Audit Skill Points [{skill_point_threshold}]"
msgstr "Auditoría de Miembro Puntos de Habilidad [{skill_point_threshold}]"

#: memberaudit_securegroups/models.py:735
msgid "Any"
msgstr ""

#: memberaudit_securegroups/models.py:736
msgid "Mains only"
msgstr ""

#: memberaudit_securegroups/models.py:737
msgid "Alts only"
msgstr ""

#: memberaudit_securegroups/models.py:742
#, fuzzy
#| msgid ""
#| "Users must possess all of the skills in <strong>one</strong> of the "
#| "selected skillsets."
msgid ""
"Users must have a character who possess all of the skills in <strong>one</"
"strong> of the selected skill sets."
msgstr ""
"Los usuarios deben tener todas las habilidades en <strong>uno</strong> de "
"los conjuntos de habilidades seleccionados."

#: memberaudit_securegroups/models.py:751
msgid "Specify the type of character that needs to have the skill set."
msgstr ""

#: memberaudit_securegroups/models.py:789
msgid "Member Audit Skill Set"
msgstr "Auditoría de Miembro Conjunto de Habilidades"

#: memberaudit_securegroups/models.py:874
#, fuzzy
#| msgid "Member Audit Skill Set"
msgid "Member Audit Time in Corporation Filter"
msgstr "Auditoría de Miembro Conjunto de Habilidades"
